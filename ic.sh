set -x
dir=/opt/docker/enjoy

# build web
cd $dir/admin
pwd
nvm use 10.15.0
rm -rf /opt/docker/enjoy/admin/build


npm install

npm run build
ls $dir/admin/build/
cd $dir/admin
ls -la $dir/admin/build
