export default {
    client_name: "Enjoy the Run",
    project_title: "Enjoy the Run",
    version: "1.0.0",
    
    firebase: {
        apiKey: process.env.FIREBASE_API_KEY || 'AIzaSyDPVCFJl3ZlBY44GrVQWIh3tlEwl0EuGcg',
        authDomain: process.env.FIREBASE_AUTH_DOMAIN || 'enjoy-the-run.firebaseapp.com',
        databaseURL: process.env.FIREBASE_DATABASE_URL || 'https://enjoy-the-run.firebaseio.com',
        projectId: process.env.FIREBASE_PROJECT_ID || 'enjoy-the-run',
        storageBucket: process.env.FIREBASE_STORAGE_BUCKET || 'enjoy-the-run.appspot.com',
        messagingSenderId: process.env.FIREBASE_MSG_SENDER_ID || '761345127242',
        appId: process.env.FIREBASE_APP_ID || '1:761345127242:web:9f8cd12b38feecc9',
    },

    cloudFunction: (cmd) => {
        return `https://us-central1-enjoy-the-run.cloudfunctions.net/${cmd}`
    }
}