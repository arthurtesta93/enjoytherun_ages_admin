import React from 'react'

/* React Router */
import { IndexRoute, Router, Route, browserHistory } from 'react-router'

/* Library helpers */
import Authentication from './lib/Authentication'

/* Components */
import Login from './components/Login/Index'

import RestrictedAreaContainer from './components/RestrictedArea/'

import Dashboard from './components/RestrictedArea/Dashboard'
import ChangePassword from './components/RestrictedArea/ChangePassword'

import Athletes from './components/RestrictedArea/Athletes/Index'
import AthletesEdit from './components/RestrictedArea/Athletes/Edit'
import Events from './components/RestrictedArea/Events/Index'
import EventsEdit from './components/RestrictedArea/Events/Edit'
import EventView from './components/RestrictedArea/Events/View.js'
import Admins from './components/RestrictedArea/Admin/Index';
import AdminEdit from './components/RestrictedArea/Admin/Edit'
const AppRouter = () => {
    return (
        <Router history={browserHistory}>
            <Route path="/" component={RestrictedAreaContainer} onEnter={validateRestrictedArea}>
                <IndexRoute component={Dashboard} onEnter={validateRestrictedArea} />
                <Route path="/password" component={ChangePassword} />

                <Route path="/athletes" component={Athletes} onEnter={validateRestrictedArea} />
                <Route path="/athletes/edit/:id" component={AthletesEdit} onEnter={validateRestrictedArea} />
                <Route path="/athletes/create" component={AthletesEdit} onEnter={validateRestrictedArea} />

                <Route path="/events/view/:id" component={EventView} onEnter={validateRestrictedArea} />
                <Route path="/events" component={Events} onEnter={validateRestrictedArea} />
                <Route path="/events/edit/:id" component={EventsEdit} onEnter={validateRestrictedArea} />
                <Route path="/events/create" component={EventsEdit} onEnter={validateRestrictedArea} />

                <Route path="/admins" component={Admins} onEnter={validateRestrictedArea} />
                <Route path="/admins/edit/:id" component={AdminEdit} onEnter={validateRestrictedArea} />
                <Route path="/admins/create" component={AdminEdit} onEnter={validateRestrictedArea} />

            </Route>
            <Route path="/login" component={Login} onEnter={validateAlreadyLogged} />

        </Router>
    )
}

/* Route validators based on login status */
function validateRestrictedArea(nextState, replace) {
    if (nextState !== undefined) {
        if (!Authentication.isAuthenticated()) {
            replace({
                pathname: '/login',
                state: {nextPathname: nextState.location.pathname}
            })
        }
    }
}

function validateAlreadyLogged(nextState, replace) {    
    if (nextState !== undefined) {
        if (Authentication.isAuthenticated()) {
            replace({
                pathname: '/',
                state: { nextPathname: nextState.location.pathname }
            })
        }
    }
}

export default AppRouter