import firebase from 'firebase'
import Config from '../config'

const db = firebase.firestore()

export default class AthlService {

    static async get(id) {
        let doc = await db.collection("users").doc(id).get()
        
        if (doc.exists)
            return doc.data()

        return null
    }
      
    static async getAll() {
        let query = await db.collection("users")
            .where("isAdmin", "==", false)
            .where("deleted", "==", false)
            .get()
            
        return query.docs.map(doc => {
            const data = doc.data()

            return { _id : doc.id, ...data }
        })
    }

    static async deleteAthlete(id,requesterId) {
        const params = {
            id: id,
            reqId: requesterId
        }

        return await fetch(
            Config.cloudFunction("deleteUser"), {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(res => res.json())
        .then(({result}) => {
            if (result) {
                this.updateRegister({ deleted: true }, id);  
                return true;
            }

            return false;

        })
    }

    static async athleteRegister(values,email,password, requesterId) {
        const params = {
            reqId: requesterId,
            email: email,
            password: password
        }

        return await fetch(
            Config.cloudFunction("createUser"), {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        })
        .then(res => res.json())
        .then(async result => {
            if(result.result){
                // atualiza o banco criando na database o user criado com seus dados
                await db.collection("users").doc(result.id).set(values)
                return true;
            }
            return false;

        })
    }

    static async updateRegister(values, id) {
        await db.collection("users").doc(id).update(values);

        return true;
    }

    static async updatePassword(password, id, requesterId) {
        const params = {
            id: id,
            newPassword: password,
            reqId: requesterId
        }

        return await fetch(
            Config.cloudFunction("updatePassword"), {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
        })
            .then(res => res.json())
            .then(async res => res.result);
    }

    static async increaseAmountOfTrainings(id){
        const athlete = await AthlService.get(id)
        const numeroDeTreinos = athlete.amountOfTrainings

        return this.updateRegister({amountOfTrainings: numeroDeTreinos + 1}, id)
    }

    static async decreaseAmountOfTrainings(id){

        const athlete =  await AthlService.get(id)
        const numeroDeTreinos = athlete.amountOfTrainings

        return this.updateRegister({amountOfTrainings: numeroDeTreinos - 1}, id)
    }

    static async increaseAmountOfCompletedTrainings(id){

        const athlete = await AthlService.get(id)
        const numeroDeTreinosCompletos = athlete.completedTrainings

        return this.updateRegister({completedTrainings: numeroDeTreinosCompletos + 1}, id)
    }
    

}