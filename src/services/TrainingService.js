import firebase from 'firebase'
import AthlService from '../services/AthlService'
import MonthSchema from '../schemas/Month'
import TrainingSchema from '../schemas/Training'

const db = firebase.firestore()

export default class TrainingService {
    static async getMonths(id) {
        const query = await db.collection(`trainings/${id}/months`).get()
        const months = []

        query.forEach(doc => {
            if (doc.exists) {
                months.push(new MonthSchema({
                    id: doc.id,
                    ...doc.data()
                }))
            }
        })

        return months
    }

    static async getTrainings(userId, monthId) {
        const query = await db.collection(`trainings/${userId}/months/${monthId}/trainingItems`).get()

        const trainings = []

        query.forEach(doc => {
            if (doc.exists) {
                trainings.push(new TrainingSchema({
                    id: doc.id,
                    ...doc.data()
                }))
            }
        })

        return trainings
    }

    //###################################################
    //CRUD DE MESES
    //###################################################

    static async createMonth(userId, monthSchema) {
        return await db.collection(`trainings/${userId}/months`)
            .add(monthSchema.getBody());
    }

    //###################################################
    //CRUD DE TREINOS
    //###################################################
    static async createTraining(userId, monthId, training) {
        AthlService.increaseAmountOfTrainings(userId)
        
        return await db.collection(`trainings/${userId}/months/${monthId}/trainingItems`)
            .add(training.getBody())
         
    }

    static async updateTraining(userId, monthId, training) {
        const body = training.getBody()
        const id = body.id

        delete body.id

        return await db.collection(`trainings/${userId}/months/${monthId}/trainingItems`)
            .doc(id)
            .update(body)
    }

    static async deleteTraining(userId, monthId, training) {
        const body = training.getBody()
        
        AthlService.decreaseAmountOfTrainings(userId)

        return await db.collection(`trainings/${userId}/months/${monthId}/trainingItems`)
            .doc(body.id)
            .delete()
    }
}