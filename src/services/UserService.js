import firebase from 'firebase'

const db = firebase.firestore()

export default class UserService {
    static async getUserById(id) {
        let doc = await db.collection("users").doc(id).get()
        
        if (doc.exists)
            return doc.data()

        return null
    }

    static async getAll() {
        let query = await db.collection("users").get()
        
        return query.docs.map(doc => doc.data())
    }
}