import firebase from 'firebase'
import DateLib from '../lib/Date';

const db = firebase.firestore()

//############################################################
//#####################TO BE IMPLEMENTED#####################
//############################################################


export default class EventService {

    static async get(id) {
        let doc = await db.collection("feed").doc(id).get()
        
        if (doc.exists)
            return doc.data()

        return null
    }
      
    static async getAll() {
        let query = await db.collection("feed")
            .where("type", "==", "event")
            .get()
            
        return query.docs.map(doc => {
            const data = doc.data();
            data.eventDate = DateLib.TimestampToDate(data.eventDate)
            return { _id : doc.id, ...data }
        })
    }

    static async getEventLikes(eventId){
        let query = await db.collection(`eventLikes/${eventId}/users`).get()

        const likes = []

        query.forEach(doc => {
            if (doc.exists) {
                likes.push(doc.id)
            }
        })

        return likes

    }

    static async deleteEvent(id) {
        return await this.updateEvent({ deleted: true }, id);
    }

    static async registerEvent(values) {
        return await db.collection("feed").add(values)
    }

    static async updateEvent(values, id) {
        return await db.collection("feed").doc(id).update(values);
    }

}