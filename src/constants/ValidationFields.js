module.exports = {
    Required:  (value, all, props, name) =>{
        if(typeof value === "string") value = value.trim();
        return value && value !== 0 ? undefined : 'O campo é obrigatório';
    },

    DropdownRequired:  (value, all, props, name) =>{
        return value !== undefined ? undefined : 'Selecione uma das opções acima'
    },

    MaxLength: max => value =>
        value && value.length > max ? `O valor não pode ser superior a '${max}'` : undefined,
    
    Number: value =>
        value && isNaN(Number(value)) ? 'Informe um número válido' : undefined,


    MinValue: min => value =>
        value && value < min ? `O valor não pode ser inferior a '${min}'` : undefined,

    GreaterOrEqualToOne: value =>{  value = Number(value);  
        return Number.isInteger(value) ? (value >= 1 ? undefined : 'O valor não pode ser inferior a um'): undefined
    },

    Email: value => {
       return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
            'Informe um e-mail válido' : undefined
        },

    Date: value => {
       return value && !/^(0?[1-9]|[12][0-9]|3[01])[/-](0?[1-9]|1[012])[/-]\d{4}$/.test(value) ?
            'Informe uma data válida' : undefined
        },

    OptionalPassword: value =>
        value ? (value.length >= 8 &&
        /[A-Z]/.test(value) &&
        /[a-z]/.test(value) &&
        /\d/.test(value) ? undefined : 'A senha necessita ter 1 letra maiúscula, 1 minúscula, 1 número e mínimo 8 caracteres') : undefined,

    Password: value =>
        value ? (value.length >= 8 &&
        /[A-Z]/.test(value) &&
        /[a-z]/.test(value) &&
        /\d/.test(value) ? undefined : 'A senha necessita ter 1 letra maiúscula, 1 minúscula, 1 número e mínimo 8 caracteres') : undefined, //'O campo é obrigatório' 
}