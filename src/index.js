import React from 'react'
import ReactDOM from 'react-dom'

import 'whatwg-fetch'

/* Redux */
import { Provider } from 'react-redux'
import reduxStore from './lib/ReduxStore'
import AppRouter from './routes'

import { MuiThemeProvider } from 'material-ui/styles'
import { muiTheme } from './theme/'

ReactDOM.render(
    <Provider store={ reduxStore }>
        <MuiThemeProvider muiTheme={ muiTheme }>
            <div className='contentWrapper'>
                <AppRouter />
            </div>
        </MuiThemeProvider>
    </Provider>,
document.getElementById('root'))