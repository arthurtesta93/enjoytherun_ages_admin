

export default class String {

    static Nl2Br(str) {
        str = str.replace(/\r?\n/g, `<br/>`);
        return str;
    }
}