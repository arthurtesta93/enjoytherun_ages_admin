import Auth from './Authentication';
import Config from '../config.js';
import request from 'superagent';

function _handleRequest(requestType, url, data, callback, errorCallback) {
    let req = requestType(`${Config.public_api_url}/${Auth.getClientKey()}${url}`);

    if (data !== null)
        req.send(data);

    req.end((err, res) => {
        if (err){
            if(res && (res.status === 500 || res.status === 422 || res.status === 401 ))
                return errorCallback(res.body)

            alert("Erro desconhecido ao processar requisição.");
        }
        else
            callback(res.body);
    });
}

export default {
    post(url, data = {}, callback = () => {}, errorCallback = () => {}) {
        _handleRequest(request.post, url, data, callback, errorCallback);
    },

    put(url, data = {}, callback = () => {}, errorCallback = () => {}) {
        _handleRequest(request.put, url, data, callback, errorCallback);
    },

    delete(url, callback = () => {}, errorCallback = () => {}) {
        _handleRequest(request.delete, url, null, callback, errorCallback);
    },

    get(url, callback = () => {}, errorCallback = () => {}) {
        _handleRequest(request.get, url, null, callback, errorCallback);
    },
}