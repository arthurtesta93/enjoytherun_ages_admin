import React, { Component } from 'react';

import BlockUI from 'react-block-ui';

export default class RequestBlockUI extends Component {
    render() {
        return (
            <BlockUI
                tag="div"
                renderChildren={false}
                {...this.props} >
                {this.props.children}
            </BlockUI>
        );
    }
}