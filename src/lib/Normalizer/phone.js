
const normalizerPhone = (value, previousValue) => {
    if (!value) 
        return value
    
    const onlyNums = value.replace(/[^\d]/g, '')
    if (onlyNums.length <= 4) {
        return onlyNums.slice(0, 4)
    }
    if (onlyNums.length <= 9) {
        return onlyNums.slice(0, 4) + ' - ' + onlyNums.slice(4)
    }
    if (onlyNums.length <= 11) {
        return '(' + onlyNums.slice(0, 2) + ') ' + onlyNums.slice(2, 6) + ' - ' + onlyNums.slice(6)
    }
    return "+ " + onlyNums.slice(0, 2) + ' (' + onlyNums.slice(2, 4) + ') ' + onlyNums.slice(4, 8) + ' - ' + onlyNums.slice(8, 13)
}

export default normalizerPhone