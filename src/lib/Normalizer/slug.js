const normalizerSlug = (str, previousValue) => {
    if (!str)
        return str;

    str = str.replace(/^\s+|\s+$/g, '');
    str = str.toLowerCase();

    let from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
    let to = "aaaaaeeeeeiiiiooooouuuunc______";

    for (let i=0, l=from.length ; i<l ; i++)
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

    str = str.replace(/[^a-z0-9 -]/g, '')
        .replace(/\s+/g, '_')
        .replace(/-+/g, '_');

    //remove leading numbers
    str = str.replace(/^\d+/, '');

    return str;
}

export default normalizerSlug