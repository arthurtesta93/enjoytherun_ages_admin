import firebase from 'firebase'
import Config from '../config'

firebase.initializeApp(Config.firebase)

const db = firebase.firestore()
const firebaseAuth = firebase.auth()

export default class Firebase {
    static getAll = (node, callback, size = 20) => {

        return db.collection("users").get()
    }

    static getCurrentUser = () => {
        return firebaseAuth.currentUser
    }

    static login = (email, password) => {
        return firebaseAuth.signInWithEmailAndPassword(email, password)
    }

    static logout = () => {
        return firebaseAuth.signOut()
    }

    static onAuthChange = (callbackLogin, callbackLogout) => {
        firebaseAuth.onAuthStateChanged(authUser => {
            if (!authUser) {
                callbackLogout(authUser)
            } else {
                callbackLogin(authUser)
            }
        })
    }

}