import React, {PureComponent} from 'react';

import { Checkbox as MaterialUICheckbox } from 'material-ui'

export default class Checkbox extends PureComponent {
    render() {
        return (
            <MaterialUICheckbox
                label={this.props.label}
                checked={this.props.input.value ? true : false}
                onCheck={this.props.input.onChange} />
        );
    }
}