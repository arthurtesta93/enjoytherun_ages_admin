import React from 'react';
import { RaisedButton, FlatButton, Dialog } from 'material-ui';
import PropTypes from 'prop-types';
import Radium from 'radium';
import FaIconComponent from './iconFaComponent';

var iconsFromJson = require("./iconInfo4.json");

class MaterialUiIconPicker extends React.PureComponent {

	styles;

	constructor(props) {
		super(props);

		this.state = {
			pickerDialogOpen: false,
			_icons: [],
			icons: [],
			icon: null
		};
		
		this.clearSearch = this.clearSearch.bind(this);
		this.filterList = this.filterList.bind(this);
	}

	getStyles() {
		const backgroundBox = {
			backgroundColor: 'rgb(224, 224, 224)',
			borderRadius: 5,
			height: 120,
			opacity: 0,
			position: 'absolute',
			top: 0,
			left: 0,
			width: '100%',
			transitionDelay: 'initial'
		};

		const selectedBackgroundBox = Object.assign({}, backgroundBox);
		selectedBackgroundBox.opacity = 0.2;

		return {
			iconsGrid: {
				display: 'flex',
				flexWrap: 'wrap'
			},
			iconsItem: {
				textAlign: 'center',
				width: '25%',
				flexGrow: 1,
				marginBottom: 10,
				position: 'relative',
				height: 120,
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
				flexDirection: 'column',
				cursor: 'pointer'
			},
			iconsItemCaption: {
				textTransform: 'uppercase',
				fontSize: 10,
				whiteSpace: 'nowrap',
				overflow: 'hidden',
				textOverflow: 'ellipsis',
				position: 'relative',
				zIndex: 2,
				maxWidth: 100
			},
			iconsItemIcon: {
				color: 'rgb(117, 117, 117)',
				fontSize: 48,
				width: 48,
				height: 48,
				marginBottom: 10
			},
			iconsItemIconSmall: {
				color: 'rgb(117, 117, 117)',
				fontSize: 35,
				width: 30,
				height: 30,
				bottom: 4,
				position: "absolute"

			},
			backgroundBox: backgroundBox,
			selectedBackgroundBox: selectedBackgroundBox,
			header: {
				wrapper: {
					display: 'flex',
					flexDirection: 'column',
					paddingBottom: 0,
					paddingLeft: 0,
					paddingRight: 0
				},
				input: {
					flex: 1,
					border: 'none',
					padding: 15,
					fontSize: 17,
					margin: '0 40',
					':focus': {
						outline: 'none'
					}
				},
				icons: {},
				title: {
					paddingLeft: 24,
					paddingTop: 0,
					paddingRight: 24,
					textTransform: 'uppercase'
				},
				search: {
					boxShadow: 'rgba(0, 0, 0, 0.14) 0px 4px 5px 0px, rgba(0, 0, 0, 0.12) 0px 1px 10px 0px, rgba(0, 0, 0, 0.2) 0px 2px 4px -1px',
					display: 'flex',
					marginTop: 10,
					position: 'relative',
					zIndex: 4,
					background: '#fff',
					alignItems: 'center',
					paddingLeft: 10,
					paddingRight: 10
				},
				searchIcon: {
					color: '#ddd'
				},
				closeIcon: {
					cursor: 'pointer',
					color: '#555'
				}

			}
		};
	}

	componentDidMount() {
		const styles = this.getStyles();
		const icons = iconsFromJson.map(( icon, index ) =>{
			return {
				...icon,
				component: ( 
				<div key = {index}  onClick={() => { this.select(icon)} } style = {styles.iconsItem}>
				<FaIconComponent 
					selectedBackgroundBox = {styles.selectedBackgroundBox}
					backgroundBox = {styles.backgroundBox}
					iconsItemCaption = {styles.iconsItemCaption}
					iconsItemIcon = {styles.iconsItemIcon}
					select={this.select} 
					selected = {this.state.selected}
					icon = {icon}
				/> 
				</div>)
			}
		})
		this.showIcons(icons);
	}

	componentDidMount() {
	}
	
	showIcons(icons) {
		this.setState({pickerDialogOpen: this.state.pickerDialogOpen, _icons: icons, icons: icons});
	}

	handleOpen() {
		this.setState({
			pickerDialogOpen: true,
		});
	};

	handleClose() {
		this.setState({
			pickerDialogOpen: false,
			icons: this.state._icons,
			didSearch: false
		});
	};

	pickAndClose() {
		//this.props.onPick(this.state.selected);
		this.props.input.onChange(this.state.selected.class)
		this.handleClose();
	}

	select(icon) {
		this.setState({
			pickerDialogOpen: this.state.pickerDialogOpen,
			selected: icon,
		}, this.pickAndClose.bind(this));
	}

	filterList(event) {

		if (event.target.value.toLowerCase().length === 0) {
			this.clearSearch();
		} else {
			let updatedList = this.state._icons;
			updatedList = updatedList.filter(function (item) {
				return item.label.search(event.target.value.toLowerCase()) !== -1;
			});

			this.setState({
				icons: updatedList,
				didSearch: true
			});

		}
	}

	clearSearch(searchInput) {
		document.getElementById("iconSearch").value = '';

		this.setState({
			icons: this.state._icons,
			didSearch: false
		});
	}

	render() {
		const styles = this.getStyles();

		const actions = [
			<FlatButton
				label={this.props.cancelLabel}
				primary
				onClick={this.handleClose.bind(this)}
			/>,
		];

		const icons = this.state.icons.map((icon, index) => { return icon.component } )
		return (
				<div>
					<p className="input-label" style={{marginBottom: "5px"}} >{this.props.label}</p>
					<RaisedButton onClick={this.handleOpen.bind(this)} label={this.props.label} secondary/>
					{this.props.input.value &&
					<div className="mgl15" style={{display: "inline-block"}}>
						<i style={ styles.iconsItemIconSmall} className={"fa "+ this.props.input.value}></i>
					</div>
					}
					<Dialog
						autoScrollBodyContent
						title={ <TitleComponent styles={styles} modalTitle={this.props.modalTitle} filterList={this.filterList} clearSearch={this.clearSearch}/> }
						actions={actions}
						modal={false}
						open={this.state.pickerDialogOpen}
						onRequestClose={this.handleClose.bind(this)}
					>
						<div style={styles.iconsGrid}>{icons}</div>
					</Dialog>
				</div>
		);
	}
}

const TitleComponent = ({styles, modalTitle, filterList, clearSearch})=>{
	return (
		<div style={styles.header.wrapper} className="mgt10">
				<h3 style={styles.header.title}>{modalTitle}</h3>
				<div style={styles.header.search}>
					<input id="iconSearch" type="text" style={styles.header.input}
						placeholder="Search"
						onChange={filterList}/>
					
			</div>
		</div>
	)
}

MaterialUiIconPicker.propTypes = {
	cancelLabel: PropTypes.string,
	label: PropTypes.string,
	modalTitle: PropTypes.string,
	pickLabel: PropTypes.string
};

MaterialUiIconPicker.defaultProps = {
	cancelLabel: 'Cancel',
	label: 'Pick icon',
	modalTitle: 'Icon Picker',
	pickLabel: 'Pick'
};

export default Radium(MaterialUiIconPicker);