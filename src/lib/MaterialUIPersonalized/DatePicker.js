import React, {PureComponent} from 'react';

import {DatePicker as MaterialUIDatePicker} from 'material-ui';
import { defaultColors } from '../../theme/'

import moment from 'moment';

export default class DatePicker extends PureComponent {
    render() {
        let {input, meta, onChange, ...rest} = this.props;
        if(!this.props.input.value){
            this.props.input.onChange( new Date());
        }
        return (
            <MaterialUIDatePicker autoOk={true}
                name={this.props.name}
                floatingLabelFixed={true}
                floatingLabelStyle={{color: defaultColors.primary}}
                errorText={meta.touched && meta.error}
                floatingLabelText={this.props.floatingLabelText}
                onChange={(ev, vl) => {
                    input.onChange(vl)
                    if (this.props.onChange)
                        onChange(vl);
                }}
                defaultDate = {this.props.input.value ?  new Date(this.props.input.value): new Date()}
                formatDate={(d => {
                    return moment(d).format("DD/MM/YYYY")
                })}
                mode="landscape"
                fullWidth={true}
                {...rest} />
        );
    }
}