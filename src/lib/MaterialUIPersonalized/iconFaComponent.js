import React, { PureComponent } from 'react';

class iconFaComponent extends PureComponent {
    render() {
        return (
        <div>
            <i style={ this.props.iconsItemIcon} className={"fa "+ this.props.icon.class}></i>
            <div style={ this.props.iconsItemCaption}>{ this.props.icon.label}</div>
        </div>
        );
    }
}

export default iconFaComponent;


// import React, { Component } from 'react';

// const iconFaComponent = (props) => {
    
//     return (
//         <div>
//             <div style={ props.selected &&  props.selected.label ===  props.icon.label ?  props.selectedBackgroundBox :  props.backgroundBox}></div>
//             <i style={ props.iconsItemIcon} className={"fa "+ props.icon.class}></i>
//             <div style={ props.iconsItemCaption}>{ props.icon.label}</div>
//         </div>
//     );
    
// }

// export default iconFaComponent;