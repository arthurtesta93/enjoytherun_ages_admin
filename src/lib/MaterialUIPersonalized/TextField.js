
import TextField from 'material-ui/TextField'
import createComponent from './createComponent'
import mapError from './mapError'

import { defaultColors } from '../../theme/'


let component =   createComponent(TextField, ({ defaultValue, ...props }) =>({
    ...mapError(props),
    floatingLabelFixed: !props.floatingLabelFixed ? true : false,
    floatingLabelStyle: props.floatingLabelStyle? props.floatingLabelStyle :{ color: defaultColors.primary },
    fullWidth: !props.fullWidth ? true : false  
})
)

export default component
