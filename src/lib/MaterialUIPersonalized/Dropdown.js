import React, {PureComponent} from 'react';

import { defaultColors } from '../../theme/'

import { MenuItem, SelectField } from 'material-ui'

export default class Dropdown extends PureComponent {
    render() {
        return (
            <SelectField
                name={this.props.name}
                {...this.props.input}
                floatingLabelFixed={true}
                floatingLabelStyle={{ color: defaultColors.primary }}
                fullWidth={true}
                errorText={this.props.meta.touched && this.props.meta.error}
                onChange={(event, index, value) => this.props.input.onChange(value)}
                floatingLabelText={this.props.label}>
                {this.options}
            </SelectField>
        );
    }

    get options() {
        if (this.props.data !== undefined) {
            return this.props.data.map(option => {
                return (
                    <MenuItem key={option[this.props.dataKey]} 
                      value={option[this.props.dataKey]}
                      primaryText={option[this.props.dataLabel]} />
                );
            });
        }

        return [];
    }
}