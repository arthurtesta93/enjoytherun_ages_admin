import React, { PureComponent } from 'react';

import MaterialUIIconButton from 'material-ui/IconButton';

export default class IconButton extends PureComponent {
    render() {
        return (
            <MaterialUIIconButton iconStyle={{width: 20, height: 20}}
                style={{width: 30, height: 5, padding: 0}}
                {...this.props}>
                {this.props.children}
            </MaterialUIIconButton>
        );
    }
}