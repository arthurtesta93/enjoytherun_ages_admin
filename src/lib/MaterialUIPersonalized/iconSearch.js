import React, { Component } from 'react';

class iconSearch extends Component {
    render() {
        return (
            <div>
                <h3>{this.props.modalTitle}</h3>
                <div style={this.props.header.search}>
                    <input ref="searchInput" type="text" style={this.props.styles.header.input}
                        placeholder="Search"
                        onChange={this.props.handleChange}/>
                    {this.state.didSearch && 
                    <button onClick={this.props.handleClick} className="material-icons">close</button>}
                </div>
            </div>
        );
    }
}

export default iconSearch;