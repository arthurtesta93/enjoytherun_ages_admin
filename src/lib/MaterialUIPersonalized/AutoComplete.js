import { AutoComplete } from 'material-ui'


import createComponent from './createComponent'
import mapError from './mapError'


let component = createComponent(AutoComplete, ({
    input: { onChange, value },
  onNewRequest,
  dataSourceConfig,
  dataSource,
  ...props
  }) => ({
    ...mapError(props),
    dataSourceConfig,
    dataSource,
    searchText: dataSourceConfig && dataSource ? (dataSource.find((item) => item[dataSourceConfig.value] === value) || { id: '-1', text: value, name: value, notValid: true })[dataSourceConfig.text] : value,
    onNewRequest: value => {
      if (props.forceDisable)
        return
      if (dataSourceConfig)
        onChange(value[dataSourceConfig.value]);
      else
        onChange(value);

      if (onNewRequest) {
        onNewRequest(value)
      }
    },
    onUpdateInput: value => {
      if (props.forceDisable)
        return

      onChange(value)
    },
  }))

export default component
