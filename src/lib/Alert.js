/*
 * Library containing helper methods do display ALERTS to the user.
 * Here we are currently using Bootstrap-Sweetalert
 * @see https://github.com/lipis/bootstrap-sweetalert
 */

export default {
    /*
     * Display an error alert
     * @param text string
     * @param config object
     * @return void
     */
    error(text, callback = (() => {}), config = {}) {
        this.call({
            text: text,
            title: "Ops",
            type: "error",
            ...config
        }, callback);
    },

    /*
     * Display a success alert
     * @param text string
     * @param config object
     * @return void
     */
    success(text, callback = (() => {}), config = {}) {
        this.call({
            title: "Ok",
            text: text,
            type: "success",
            ...config
        }, callback);
    },

    /*
     * Ask a confirmation to the user
     * @param text string
     * @param callback function
     * @param config object
     * @return void
     */
    confirm(text, callback, config = {}) {
        if (window.confirm(text))
            callback(true)
        else
            callback(false)
    },

    /*
     * Alert for ajax requests
     * @param text string
     * @param callback function
     * @param config object
     * @return void
     */
    request(text, callback, config = {}) {
        this.call({
            title: "Enviando dados",
            text: text,
            type: "info",
            showCancelButton: false,
            showConfirmButton: false,
            ...config
        }, callback);
    },

    /*
     * Underground function of alert
     * @param callback function
     * @param config object
     * @return void
     */
    call(config, callback = (() => {})) {
        //window.swal(config, callback);
        alert(config.text)
        callback()
    },

    /*
     * Underground function of alert
     * @param config object
     * @return void
     */
    callSimple(config) {
        //window.swal(config);
    },

    close() {
        //window.swal.close();
    }
}