import React, {Component} from 'react';

class Table extends Component {
    render() {
        if (!this.props.rows || !this.props.rows.length)
            return (
                <p>Não há registros para serem exibidos.</p>
            )

        return (
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        {this.renderHeader}
                    </tr>
                </thead>
                <tbody>
                    {this.renderRows}
                </tbody>
            </table>
        )
    }

    get renderHeader() {
        return this.props.columns.map(column => {
            return (
                <th key={`thead-${column.key}`}>
                    {column.label}
                </th>
            )
        })
    }

    get renderRows() {
        return this.props.rows.map((row, i) => {
            return (
                <tr key={`table-row-${i}`}>
                    {this.renderSingleRow(row, i)}
                </tr>
            )
        })
    }

    renderSingleRow = (row, id) => {
        return this.props.columns.map((column, i) => {
            return (
                <td key={`table-row-${id}-column-${i}`}>
                    {this.renderRowColumnContent(row, column)}
                </td>
            )
        })
    }

    renderRowColumnContent = (row, column) => {
        if (column.cell)
            return column.cell(row)

        return row[column.key] || ""
    }

}

export default Table