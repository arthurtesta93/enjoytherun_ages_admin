import reducers from '../reducers'
import { createStore } from 'redux'

const reduxStore = createStore(reducers);

export default reduxStore;