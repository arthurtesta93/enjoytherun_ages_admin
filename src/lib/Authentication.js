/**
 * Authentication logic lies here
 */

export default {
    getUser() {
        let id = localStorage.authId
        let name = localStorage.userName

        return { id, name };
    },

    /**
     * Retrieve id from local storage
     * @return string id
     */
    getAuthId() {
        return localStorage.authId;
    },

    /**
     * Retrieve user name info from local storage
     * @return string
     */
    getUserName() {
        return localStorage.userName;
    },

    /**
     * Clean authentication data inside Local Storage
     * @return void
     */
    logout() {
        localStorage.authId = "";
        localStorage.userName = "";
    },

    /**
     * Returns true in case the current session has not expired yet
     * @return boolean
     */
    isAuthenticated() {
        return (localStorage.authId !== "" && localStorage.authId !== undefined);
    },

    /**
     * Store session data inside Local Storage
     * @param id string
     * @param userPersonalInfo
     * @return void
     */
    login(id, userPersonalInfo) {
        localStorage.authId = id;
        localStorage.userName = userPersonalInfo.name;
    },
}