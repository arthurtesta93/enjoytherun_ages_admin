import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';

export const defaultColors = {
  primary : "#A53693",
  secondary: "#62b4b5"
}

const getTheme = () => {
  let overwrites = {
    "palette": {
        "primary1Color": defaultColors.primary,
        "accent1Color": defaultColors.secondary
    }
};
  return getMuiTheme(baseTheme, overwrites);
}

export const muiTheme = getTheme()
