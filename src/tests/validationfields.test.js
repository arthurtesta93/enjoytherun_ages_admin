import VF from '../constants/ValidationFields'

describe("Validador de e-mail", function() {
  it('deve funcionar para um e-mail válido', () => {
    expect(VF.Email("mateus@mhaas.com.br")).toEqual(undefined)
  })

  it('deve dar erro quando e-mail é inválido (não possui first part)', () => {
    expect(typeof(VF.Email("@mhaas.com"))).toEqual("string")
  })

  it('deve dar erro quando e-mail é inválido (não possui @)', () => {
    expect(typeof(VF.Email("mateusmhaas.com.br"))).toEqual("string")
  })

  it('deve dar erro quando e-mail é inválido (não possui .com)', () => {
    expect(typeof(VF.Email("mateus@mhaas"))).toEqual("string")
  })
})

describe("Validador de Required", function() {
  it('deve funcionar para string cheia', () => {
    expect(VF.Required("mateus@mhaas.com.br")).toEqual(undefined)
  })

  it('deve dar erro quando string é vazia', () => {
    expect(typeof(VF.Required(""))).toEqual("string")
  })

  it('deve dar erro quando string tem vários espaços', () => {
    expect(typeof(VF.Required("      "))).toEqual("string")
  })
})

describe("Validador de Data", function() {
  it('deve funcionar para data válida (01/01/2019)', () => {
    expect(VF.Date("01/01/2019")).toEqual(undefined)
  })

  it('deve dar erro quando data é inválida', () => {
    expect(typeof(VF.Date("32/01/2019"))).toEqual("string")
  })
})

describe("Validador de Senha Segura", function() {
  it('deve funcionar para senha segura', () => {
    expect(VF.Password("Mateus123")).toEqual(undefined)
  })

  it('deve dar erro quando senha é segura, porém pequena', () => {
    expect(typeof(VF.Password("Mat123"))).toEqual("string")
  })

  it('deve dar erro quando senha não tem letra maiúscula', () => {
    expect(typeof(VF.Password("mateus123"))).toEqual("string")
  })

  it('deve dar erro quando senha não tem número', () => {
    expect(typeof(VF.Password("mateushaas"))).toEqual("string")
  })

  it('deve dar erro quando senha não tem letra minúscula', () => {
    expect(typeof(VF.Password("MATEUS123"))).toEqual("string")
  })

  it('deve dar erro quando senha não tem letra', () => {
    expect(typeof(VF.Password("123456789"))).toEqual("string")
  })
})