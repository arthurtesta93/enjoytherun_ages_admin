import Schema from "../lib/Schema"

import DateLib from "../lib/Date"

export default new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    eventDate: { type: String, required: true, set: (value) => {
        return DateLib.dmY2Ymd(value)
    } },
    createdAt: { type: String, /*required: true,*/},
    type: { type: String, required: true, default: "event"},
    deleted: {type: Boolean, required: true, default: false}
})