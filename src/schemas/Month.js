import Schema from "../lib/Schema"

export default new Schema({
    id: { type: String },
    index: { type: Number, required: true },
    fulfilled: { type: Boolean, required: true }
})