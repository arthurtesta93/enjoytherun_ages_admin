import Schema from "../lib/Schema"
// import DateLib from "../lib/Date"

export default new Schema({
    id: { type: String },
    week: { type: Number, required: true },
    order: { type: Number, required: true },
    description: { type: String, required: true },
    createdAt: { type: String, /*required: true,*/},
    completedAt: { type: String },
    meters: { type: String },
    seconds: { type: Number }
})