import Schema from "../lib/Schema"

import DateLib from "../lib/Date"

export default new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    cpf: { type: String, required: true },
    dob: { type: String, required: true, set: (value) => {
        return DateLib.dmY2Ymd(value)
    } },
    phone: { type: String, required: true },
    isAdmin: { type: Boolean, default: false, required: true },
    deleted: { type: Boolean, default: false, required: true },
    amountOfTrainings: {type: Number, default: 0, required: true },
})