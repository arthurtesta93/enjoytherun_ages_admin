import React, { Component } from 'react'

import { defaultColors } from '../theme/'
import config from '../config'

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="footer-bottom" style={{ backgroundColor: defaultColors.primary }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-5">
                                <div className="copyright">
                                    © {this.currentYear}, All rights reserved
                                </div>
                            </div>

                            <div className="col-md-2">
                                <div className="design">
                                    Enjoy the Run v{config.version}
                                </div>
                            </div>

                            <div className="col-md-5">
                                <div className="design">
                                    Made with <span role="img" aria-label="heart">❤️</span> by AGES
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }

    get currentYear() {
        return new Date().getFullYear()
    }
}

export default Footer