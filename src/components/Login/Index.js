import React, { Component } from 'react'

/* Library helpers */
import Authentication from '../../lib/Authentication'
import Alert from '../../lib/Alert'

/* Form */
import { Field, reduxForm } from 'redux-form'
import VF from '../../constants/ValidationFields'

import RaisedButton from 'material-ui/RaisedButton'

import { renderTextField as TextField } from '../../lib/MaterialUIPersonalized'

import Firebase from '../../lib/Firebase'
import Footer from '../_Footer'

import UserService from '../../services/UserService'

class Login extends Component {
    render() {
        const { handleSubmit } = this.props

        return <div>
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div className="box-login">
                            <img src="/assets/img/client.png" className="center-block img-responsive" alt="Enjoy the Run" />

                            <form onSubmit={handleSubmit(this.submitForm)}  >
                                <Field component={TextField}
                                    name="email"
                                    type="text"
                                    autoComplete="off"
                                    hintText="Exemplo: teste@teste.com.br"
                                    floatingLabelText='Email'
                                    validate={[VF.Required, VF.Email]} />

                                <Field component={TextField}
                                    name="password"
                                    type="password"
                                    autoComplete="off"
                                    hintText={("Exemplo: Senha_357")}
                                    floatingLabelText={('Senha')}
                                    validate={[VF.Required]} />

                                <br />

                                <div className="text-center">
                                    <RaisedButton type="submit" primary={true} color="primary" label="Login" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <Footer />

        </div>
    }

    submitForm = async (bag) => {
        const { email, password } = bag

        try {
            const auth = await Firebase.login(email, password)

            const { uid } = auth.user

            const user = await UserService.getUserById(uid)

            if (user) {
                Authentication.login(uid, user)

                this.props.router.push("/")
            }
        }
        catch (e) {
            Alert.error('Email e/ou senha incorretos')
        }
        
    }
}

export default reduxForm({ form: "login" })(Login)