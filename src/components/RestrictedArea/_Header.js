/* React, Auth, Link && General imports */
import React, { Component } from 'react'
import { Link } from 'react-router'
import Authentication from '../../lib/Authentication'

/* Material import  */
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import NavigationClose from 'material-ui/svg-icons/navigation/close'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'

import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more'

/* Other imports */
import { defaultColors } from '../../theme/'

class RestrictedAreaHeader extends Component {
    constructor(props) {
        super(props)

        this.state = {
            openDrawer: false,
            openPopover: false,
            anchorEl: null,
        }
    }

    logout = () => {
        Authentication.logout()
        this.props.router.push("/login")
    }

    handleToggle = () => { this.setState({ openDrawer: !this.state.open }) }

    handleClose = () => this.setState({ openDrawer: false })

    handleClickPopover = (event) => {
        event.preventDefault()
        this.setState({
            openPopover: true,
            anchorEl: event.currentTarget,
        })
    }

    handleRequestClosePopover = () => {
        this.setState({
            openPopover: false,
        })
    }

    renderRightElement() {
        return (

            <div style={{ horizontalAlign: 'middle', display: "block" }}>

                <div style={{ color: 'white', cursor: 'pointer' }} className='col-xs-12' onClick={this.handleClickPopover}>
                    <ul className="nav navbar-nav" style={{ float: 'none' }}>
                        <li>
                            <span style={{ display: 'inline-block' }}>
                                <div className="hidden-xs" style={{ display: 'inline-block', margin: '13px 10px -3px', overflow: 'hidden', verticalAlign: 'super' }}>
                                    {Authentication.getUserName()}
                                </div>
                                <div style={{ display: 'inline-block' }}> <NavigationExpandMoreIcon style={{ color: 'white' }} /></div>
                            </span>
                        </li>
                    </ul>

                </div>

                <Popover open={this.state.openPopover}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ "horizontal": "right", "vertical": "bottom" }}
                    targetOrigin={{ "horizontal": "right", "vertical": "top" }}
                    onRequestClose={this.handleRequestClosePopover}>
                    <Menu>
                        <MenuItem className='visible-xs' style={{ backgroundColor: defaultColors.secondary, color: 'white' }}>
                            {Authentication.getUserName()}
                        </MenuItem>

                        <Link to="/password" onClick={this.handleRequestClosePopover}><MenuItem > {('Alterar Senha')} </MenuItem></Link>

                        <MenuItem onClick={this.logout} >
                            <span className="glyphicon glyphicon-log-out"></span> Logout
                        </MenuItem>
                    </Menu>
                </Popover>
            </div>

        )
    }

    renderDrawerItems() {
        return (
            <span>
                <AppBar title="Menu" iconElementLeft={<IconButton><NavigationClose /></IconButton>} onLeftIconButtonClick={this.handleClose} />
                <Link to="/"><MenuItem onClick={this.handleClose} >Home</MenuItem></Link>
                <Link to="/admins"><MenuItem onClick={this.handleClose} >Administradores</MenuItem> </Link>
                <Link to="/athletes"><MenuItem onClick={this.handleClose} >Atletas</MenuItem></Link>
                <Link to="/events"><MenuItem onClick={this.handleClose} >Eventos</MenuItem></Link>
            </span>
        )
    }

    render() {
        return (
            <div>
                <AppBar title="Enjoy the Run"
                    titleStyle={{ overflow: 'visible' }}
                    iconElementRight={this.renderRightElement()}
                    onLeftIconButtonClick={this.handleToggle} />

                <Drawer docked={false}
                    width={200}
                    open={this.state.openDrawer}
                    onRequestChange={(openDrawer) => this.setState({ openDrawer })}>
                    {this.renderDrawerItems()}
                </Drawer>
            </div>
        )
    }
}

export default RestrictedAreaHeader