import React, {Component} from 'react'
import FirebaseLib from '../../../lib/Firebase'
import { Link } from 'react-router'
import Table from '../../../lib/Table'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from '../../../lib/MaterialUIPersonalized/IconButton'
import DeleteHome from 'material-ui/svg-icons/action/delete'
import ContentCreate from 'material-ui/svg-icons/content/create'
import Alert from '../../../lib/Alert'
import AdminService from '../../../services/AdminService';


class Admins extends Component{
    constructor(props){
        super(props)

        this.state = {
            admins: []
        }
    }
    componentWillMount() {
        this.fetchAdmins()
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <ul className="breadcrumb">
                        <li><Link to="/">Home</Link></li>
                        <li className="active">{('Administradores')}</li>
                    </ul>
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        <h1>
                            <i className="glyphicon glyphicon-user"></i> {('Administradores')}
                        </h1>
                    </div>

                    <div className="col-xs-6 mgt20">
                        <Link to="/admins/create">
                            <RaisedButton className='pull-right' type="button" primary={true} label={('Novo Administrador(a)')}/>
                        </Link>
                    </div>
                </div>
                
                <section id="restrictedMainSection">
                    <div className="row">
                        <div className="col-md-12">
                            <TableContent records={this.state.athletes} columns={this.columns} />
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    get columns() {
        return [
            { key: 'name', label: 'Nome' },
            { key: 'cpf', label: 'CPF' },
            { key: 'email', label: 'E-mail' },
            { key: 'actions', label: 'Ações', cell: item => {
                return <>
                    <Link to={`/admins/edit/${item._id}`}>
                        <IconButton tooltip={("Editar")}><ContentCreate /></IconButton>
                    </Link>

                    <IconButton onClick={() => {this.displayDeleteConfirmation(item._id)}} tooltip={("Deletar")}><DeleteHome /></IconButton>
                </>
            }}
        ]
    }
    
    displayDeleteConfirmation = (id) => {
        Alert.confirm('Você tem certeza que deseja deletar este usuário ?', yes => {
            if (yes)
                this.deleteAdmin(id)
        })
            
    }

    deleteAdmin = (id) => {
        AdminService.deleteAdmin(id,FirebaseLib.getCurrentUser().uid)
            .then(result => {
                this.fetchAdmins()
                if (result){
                    Alert.success("Administrador removido com sucesso.")
                }
                else{
                    Alert.error("Você não possui permissão para esta operação")
                }
            })
            .catch(err => {

                Alert.error("Houve um erro ao remover o administrador selecionado. Contate o administrador.")
            })
    }

    fetchAdmins = () => {
        AdminService.getAll()
        .then(athletes => {
            this.setState({ athletes })
        })
        .catch(err => {
            Alert.error(err)
        })
    }
}

const TableContent = (props) => {
    if (!props.records)
        return null
    
    return <Table rows={props.records} columns={props.columns} />
}

export default Admins

