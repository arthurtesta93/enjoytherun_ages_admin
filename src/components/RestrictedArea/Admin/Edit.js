import firebase from 'firebase'
import { RaisedButton } from 'material-ui'
import Moment from 'moment'
import React, { Component } from 'react'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import VF from '../../../constants/ValidationFields'
import Alert from '../../../lib/Alert'
import FirebaseLib from '../../../lib/Firebase'
import { renderTextField } from '../../../lib/MaterialUIPersonalized'
import { normalizerCPF, normalizerDate, normalizerPhone } from '../../../lib/Normalizer'
import AdminSchema from '../../../schemas/Admin';
import AdminService from '../../../services/AdminService';


class AdminEdit extends Component {
	constructor(props) {
		super(props)

		this.edit = props.params.id ? true : false

		this.state = {
			admin: null,
			loading: false
		}
	}

	componentDidMount() {
		if (this.edit)
			this.fetchAdmin()
	}

	render() {
		const { handleSubmit } = this.props
		let title = this.edit ? "Editar" : "Criar"
		let validationPass = this.edit ? [VF.Password] : [VF.Password, VF.Required]
		let labelText = this.edit ? 'Trocar Senha' : 'Senha'

		return (
			<div className="container-fluid">
				<div className="row">
					<ul className="breadcrumb">
						<li><Link to="/">Home</Link></li>
						<li><Link to="/admins">Administradores</Link></li>
						<li className="active"> {title} Administrador</li>
					</ul>
				</div>
				<div className="row">
					<div className="col-md-12">
						<h1><i className="glyphicon glyphicon-user"></i> {title} Administrador</h1>
					</div>
				</div>

				<section>
					<form onSubmit={handleSubmit(this.handleSubmit)}>
						<div className="row">
							<div className="col-md-6">
								<Field component={renderTextField}
									name="name"
									type="text"
									validate={[VF.Required]}
									floatingLabelText='Nome'
									hintText="Ex: Lucas C." />
							</div>

							<div className="col-md-6">
								<Field component={renderTextField}
									name="cpf"
									type="text"
									validate={[VF.Required]}
									hintText="Ex: 999.999.999-99"
									floatingLabelText='CPF'
									normalize={normalizerCPF} />
							</div>
						</div>

						<div className="row">
							<div className="col-md-6">
								<Field component={renderTextField}
									name="email"
									type="text"
									validate={[VF.Required, VF.Email]}
									floatingLabelText='E-mail'
									hintText="Ex: lucas@gmail.com" />
							</div>

							<div className="col-md-6">
								<Field component={renderTextField}
									name="phone"
									type="text"
									validate={[VF.Required]}
									hintText="Ex: (54)99999-9999"
									floatingLabelText='Celular'
									normalize={normalizerPhone} />
							</div>
						</div>

						<div className="row">
							<div className="col-md-6">
								<Field component={renderTextField}
									name="password"
									type="password"
									validate={validationPass}
									floatingLabelText={labelText}
									hintText="Ex: Senha_357" />
							</div>

							<div className="col-md-6">
								<Field component={renderTextField}
									name="dob"
									type="text"
									validate={[VF.Required, VF.Date]}
									hintText="Ex: 04/05/1977"
									floatingLabelText='Data de Nascimento'
									normalize={normalizerDate} />
							</div>
						</div>

						<br />

						<div className="row">
							<div className="col-md-12 text-center">
								{this.renderSubmitButton}
							</div>
						</div>
					</form>

				</section>
			</div>
		)
	}
	
	get renderSubmitButton() {
		if (this.state.loading)
			return (
				<div style={{marginTop: "50px", textAlign: "center"}}>
					<img src="/assets/img/loader.gif" alt='' style={{height: "40px"}} />
				</div>
			)
		else
			return (
				<RaisedButton type="submit" primary={true} color="primary" label='Salvar'/>
			)
	}
	
	activateLoader = () => {
		this.setState({loading: true})

	}

	deactivateLoader = () => {
		this.setState({loading: false})
	}

	handleSubmit = async (values) => {
		this.activateLoader()

		try {
			if (this.edit)
				await this.updateAthlete(values, this.props.params.id)
			else
				await this.createAthlete(values,values.email,values.password)

			this.deactivateLoader()

			Alert.success("Registro salvo com sucesso.")
			this.props.router.push("/admins")
		}
		catch (e) {
			this.deactivateLoader()
			
			Alert.error("Erro no registro.")
		}
	}

	updateAthlete = async (values, id) => {
		const athlete = new AdminSchema(values)
		const uid = FirebaseLib.getCurrentUser().uid

		if (this.edit) {
			const formData = athlete.getBody();
			const payload = new AdminAdapter(formData).toModel();

			if (values.password)
				await AdminService.updatePassword(values.password, id, uid);

			await AdminService.updateRegister(payload, id, uid);
		}
	}

	createAthlete = async (values, email, password) => {
		const athlete = new AdminSchema(values)

		await AdminService.adminRegister(athlete.getBody(),email,password,FirebaseLib.getCurrentUser().uid)
	}

	fetchAdmin = () => {
		const { id } = this.props.params;

		AdminService.get(id)
			.then(result => {
				const athlete = new AdminAdapter(result).fromModel();
				this.props.initialize(athlete);
			})
			.catch(err => {
				Alert.error("Houve um erro ao recuperar o atleta selecionado. Contate o administrador.")
			})
	}
}

export default reduxForm({ form: "athlete" })(AdminEdit)

class AdminAdapter {
	adaptee = null;

	constructor(adaptee) {
		this.adaptee = adaptee;
	}

	toModel() {
		const dob = this.adaptee.dob;
		const dobTimestamp = firebase.firestore.Timestamp.fromDate(dob);
		const result = {
			...this.adaptee,
			dob: dobTimestamp
		};
		return result;
	}

	fromModel() {
		const dob = new Date(this.adaptee.dob.seconds * 1000);
		const dobString = Moment(dob).format('DD/MM/YYYY');
		const result = {
			...this.adaptee,
			dob: dobString
		};
		return result;
	}

}