import React, {Component} from 'react'
import FirebaseLib from '../../../lib/Firebase'
import { Link } from 'react-router'
import Table from '../../../lib/Table'
import RaisedButton from 'material-ui/RaisedButton'
import IconButton from '../../../lib/MaterialUIPersonalized/IconButton'
import DeleteHome from 'material-ui/svg-icons/action/delete'
import ContentCreate from 'material-ui/svg-icons/content/create'
import Eye from 'material-ui/svg-icons/image/remove-red-eye'
import Alert from '../../../lib/Alert'
import EventService from '../../../services/EventService';

class Events extends Component {
    constructor(props) {
        super(props)

        this.state = {
            events: [],
        }
    }

    componentDidMount() {
        this.fetchEvents()
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <ul className="breadcrumb">
                        <li><Link to="/">Home</Link></li>
                        <li className="active">{('Eventos')}</li>
                    </ul>
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        <h1>
                            <i className="glyphicon glyphicon-calendar"></i> {('Eventos')}
                        </h1>
                    </div>

                    <div className="col-xs-6 mgt20">
                        <Link to="/events/create">
                            <RaisedButton className='pull-right' type="button" primary={true} label={('Cadastrar Evento')}/>
                        </Link>
                    </div>
                </div>
                
                <section id="restrictedMainSection">
                    <div className="row">
                        <div className="col-md-12">
                            <TableContent records={this.state.events} columns={this.columns} />
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    get columns() {
        return [
            { key: 'title', label: 'Título' },
            { key: 'eventDate', label: 'Data do Evento' },
            { key: 'actions', label: 'Ações', cell: item => {
                return <>
                 <Link to={`/events/edit/${item._id}`}>
                        <IconButton tooltip={("Editar")}><ContentCreate /></IconButton>
                    </Link>

                    <Link to={`/events/view/${item._id}`}>
                        <IconButton tooltip={("Visualizar")}><Eye/></IconButton>
                    </Link>

                    <IconButton onClick={() => {this.displayDeleteConfirmation(item._id)}} tooltip={("Deletar")}><DeleteHome /></IconButton>
                </>
            }}
        ]
    }
    
    displayDeleteConfirmation = (id) => {
        Alert.confirm('Você tem certeza que deseja deletar este evento?', yes => {
            if (yes)
                this.deleteEvent(id)
        })
            
    }

    deleteEvent = (id) => {
        EventService.deleteEvent(id,FirebaseLib.getCurrentUser().uid)
            .then(result => {
                this.fetchEvents()
                if (result){
                    Alert.success("Evento removido com sucesso.")
                }
                else{
                    Alert.error("Você não possui permissão para esta operação")
                }
            })
            .catch(err => {

                Alert.error("Houve um erro ao remover o evento selecionado. Contate o administrador.")
            })
    }

    fetchEvents = () => {
        EventService.getAll()
        .then(events => {
            this.setState({ events })
        })
        .catch(err => {
            Alert.error(err)
        })
    }
}

const TableContent = (props) => {
    if (!props.records)
        return null
    
    return <Table rows={props.records} columns={props.columns} />
}

export default Events