import firebase from 'firebase'
import { RaisedButton } from 'material-ui'
import Moment from 'moment'
import React, { Component } from 'react'
import { Link } from 'react-router'
import { Field, reduxForm } from 'redux-form'
import VF from '../../../constants/ValidationFields'
import Alert from '../../../lib/Alert'
import { renderTextField } from '../../../lib/MaterialUIPersonalized'
import { normalizerDate } from '../../../lib/Normalizer'
import EventSchema from '../../../schemas/Event';
import EventService from './../../..//services/EventService';


class EventsEdit extends Component {
	constructor(props) {
		super(props)

		this.edit = props.params.id ? true : false

		this.state = {
			event: null,
		}
	}

	componentDidMount() {
		if (this.edit)
			this.fetchEvent()
	}

	render() {
		const { handleSubmit } = this.props
		let title = this.edit ? "Editar" : "Criar"

		return (
			<div className="container-fluid">
				<div className="row">
					<ul className="breadcrumb">
						<li><Link to="/">Home</Link></li>
						<li><Link to="/events">Eventos</Link></li>
						<li className="active"> {title} Evento</li>
					</ul>
				</div>
				<div className="row">
					<div className="col-md-12">
						<h1><i className="glyphicon glyphicon-calendar"></i> {title} Evento</h1>
					</div>
				</div>

				<section>
					<form onSubmit={handleSubmit(this.handleSubmit)}>
						<div className="row">
							<div className="col-md-6">
								<Field component={renderTextField}
									name="title"
									type="text"
									validate={[VF.Required]}
									floatingLabelText='Título do Evento'
									hintText="Ex:Corridão no Parque Farroupilha" />
							</div>

							<div className="col-md-6">
								<Field component={renderTextField}
									name="eventDate"
									type="text"
									validate={[VF.Required, VF.Date]}
									hintText="Ex: 20/12/2019"
									floatingLabelText='Data do Evento'
									normalize={normalizerDate} />
							</div>

						</div>

						<div className="row">
							<div className="col-md-12">
								<Field component={renderTextField}
									name="description"
									type="text"
									validate={[VF.Required]}
									floatingLabelText='Descrição Completa do Evento'
									hintText="Ex: Corridão ocorrerá no parque farroupilha. Tragam água e um agasalho" />
							</div>
						</div>

						<br />

						<div className="row">
							<div className="col-md-12 text-center">
								<RaisedButton type="submit" primary={true} color="primary" label='Salvar' />
							</div>
						</div>
					</form>

				</section>
			</div>
		)
	}

	handleSubmit = async (values) => {
		try {
			if (this.edit)
				await this.updateEvent(values, this.props.params.id)
			else
				await this.createEvent(values)

			Alert.success("Registro salvo com sucesso.")
			this.props.router.push("/events")


		}
		catch (e) {
			Alert.error("Erro no registro.")
		}
	}

	updateEvent = async (values, id) => {
		const event = new EventSchema(values)

		const formData = event.getBody();
		const payload = new EventAdapter(formData).toModel();
		await EventService.updateEvent(payload, id);
	}

	createEvent = async (values) => {
		const event = new EventSchema(values)

		await EventService.registerEvent(event.getBody())
	}

	fetchEvent = () => {
		const { id } = this.props.params;

		EventService.get(id)
			.then(result => {
				const event = new EventAdapter(result).fromModel();
				this.props.initialize(event);
			})
			.catch(err => {
				Alert.error("Houve um erro ao recuperar o evento selecionado. Contate o administrador.")
			})
	}
}

export default reduxForm({ form: "event" })(EventsEdit)

 

class EventAdapter {

	adaptee = null;

	constructor(adaptee) {
		this.adaptee = adaptee;
	}


	toModel() {
		const eventDate = this.adaptee.eventDate;
		const eventDateTimestamp = firebase.firestore.Timestamp.fromDate(eventDate);
		const result = {
			...this.adaptee,
			eventDate: eventDateTimestamp
		};
		return result;
	}

	fromModel() {
		const eventDate = new Date(this.adaptee.eventDate.seconds * 1000);
		const eventDateString = Moment(eventDate).format('DD/MM/YYYY');
		const result = {
			...this.adaptee,
			eventDate: eventDateString
		};
		return result;
	}

}






