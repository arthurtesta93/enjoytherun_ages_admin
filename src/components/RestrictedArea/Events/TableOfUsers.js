import React, { Component } from 'react';
import Table from '../../../lib/Table';
import Alert from '../../../lib/Alert';
import AthlService from '../../../services/AthlService';
import EventService from './../../..//services/EventService';

export default class TableOfUsers extends Component {
    constructor(props) {
        super(props)

        this.state = {
            athletes: [],
            atlLikes: [],
        }
    }

    componentDidMount() {
        this.fetchEventLikes()
        this.fetchAthletes()
    }

    render() { 
        return (
            <div>
                <h1><i className="glyphicon glyphicon-user"></i>Lista de Interessados</h1>
            <section id="restrictedMainSection">
            <div className="row">
                <div className="col-md-12">
                    <TableContent records={this.state.athletes} columns={this.columns} />
                </div>
            </div>
        </section>
        </div>
        )
    }

    get columns() {
        return [
            { key: 'name', label: 'Nome' },
            { key: 'isGoing', label: 'Interessado', cell: item => { 
                if(item.isGoing){
                    return <i className="fa fa-check" aria-hidden="true" style={{color:"green"}}></i>
                }else{
                    return <i className="fa fa-times" aria-hidden="true" style={{color:"red"}}></i>
                }
            } }
        ]
    }

    fetchAthletes = () => {
        AthlService.getAll()
        .then(athletes => {
            this.compareWithEvent(athletes)
            this.setState({ athletes })
        })
        .catch(err => {
            Alert.error(err)
        })
    }

    fetchEventLikes = () =>{
        EventService.getEventLikes(this.props.eventId)
        .then(atlLikes => {
            this.setState({ atlLikes })
        })
        .catch(err => {
            Alert.error(err)
        })
        
    }

    compareWithEvent = (athletes) =>{
        let { atlLikes } = this.state
        athletes.forEach(atl => {
            if(atlLikes.includes(atl._id)){
                atl = Object.assign(atl, {isGoing:true});
            }
                
        });
    }
}



const TableContent = (props) => {
    if (!props.records)
        return null
    
    return <Table rows={props.records} columns={props.columns} />
}

