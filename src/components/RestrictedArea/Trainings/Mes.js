import React, { Component } from 'react'
import Semana from './Semana';
import TrainingService from '../../../services/TrainingService'

export default class Mes extends Component {
    constructor(props) {
        super(props)

        this.state = {
            trainings: null
        }
    }

    componentDidMount() {
        this.fetchTrainings()
    }
    componentDidUpdate(prevProps){
        if (this.props.month !== prevProps.month)
            this.fetchTrainings();
    }

    fetchTrainings = () => {
        const { athleteId, month } = this.props

        const id = month.getBody().id

        TrainingService.getTrainings(athleteId, id)
            .then(trainings => {
                this.setState({ trainings })
            })
            .catch(() => {
                alert("erro ao buscar lista de treinos do mês")
            })
    }

    forceReloadTrainings = () => {
        this.setState({ trainings: null })

        this.fetchTrainings()
    }

    render() {
        return (
            <div className="planilha">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h3>Mês {this.props.month.getBody().index}</h3>
                    </div>
                </div>

                <br />
                {/* <Modal/> */}
                <div className="row">
                    {this.renderBody()}
                </div>
            </div>
        )
    }

    renderBody = () => {
        return this.state.trainings ? 
            this.renderSemanas() : 
            this.renderLoader()
    }

    renderLoader = () => {
        return <div style={{textAlign: "center", marginTop: "50px"}}>
            <img src="/assets/img/loader.gif" alt='' style={{height: "100px"}} />
        </div>
    }

    renderSemanas = () => {
        const { athleteId, month } = this.props
        const { trainings } = this.state
        const semanas = []

        for (let i = 1; i<= 4; i++) {
            const trainingsInWeek = trainings.filter(t => 
                t.getBody().week === i)

            semanas.push(
                <div className="col-md-3" key={`m-${month}-s-${i}`}>
                    <Semana numero={i} 
                        month={month}
                        athleteId={athleteId}
                        forceReload={this.forceReloadTrainings}
                        trainings={trainingsInWeek} />
                </div>
            )
        }

        return semanas
    }
}