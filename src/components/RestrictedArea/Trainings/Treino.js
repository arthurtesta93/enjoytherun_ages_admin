import React, { Component } from 'react'
import TrainingService from '../../../services/TrainingService';
import LibDate from '../../../lib/Date';
import libString from '../../../lib/String';
import Modal from 'react-responsive-modal';

var parse = require('html-react-parser');

export default class Treino extends Component {
	constructor(props) {
		super(props);

		const treino = props.treino

		this.state = {
			treino,
			edit: props.forceEdit || false,
			text: treino.getBody().description
		}
	}

	render() {
		return (
			<div className="treino">
				{this.renderTreino()}
			</div>
		)
	}

	renderTreino = () => {
		if (!this.state.edit)
			return this.renderTreinoText();

		return this.renderForm();
	}

	get renderCommandCompletedIcon() {
		const body = this.state.treino.getBody()

		if (!body.completedAt)
			return (
				<i className="fa fa-times-circle-o text-danger" title="Treino pendente"></i>
			)
		else
			return <>
				<TreinoModal treino={body} />

				<br />

				<i className="fa fa-check-square-o text-success"
					title={`Treino finalizado em ${LibDate.formatDate(body.completedAt.toDate())} 
					às ${LibDate.DateToTime(body.completedAt.toDate())}`}></i>
			</>
	}

	get renderCommands() {
		return <>
			{this.renderCommandCompletedIcon}

			<br />

			<i className="fa fa-trash" style={{cursor: "pointer"}} onClick={this.deleteTreino}></i>
		</>
	}

	renderTreinoText = () => {
		return (
			<div className="row">
				<div className="col-md-10" style={{ wordBreak: "break-all" }} onClick={this.switchEdit}>
					{ parse(libString.Nl2Br(this.state.text)) }
				</div>

				<div className="col-md-2 text-right">
					{this.renderCommands}
				</div>
			</div>
		)
	}

	renderForm = () => {
		return (
			<form>
				<textarea className="textAreaSize" autoFocus={true} onChange={this.editText} onBlur={this.saveTraining} value={this.state.text}></textarea>
			</form>
		)
	}

	saveTraining = () => {
		this.switchEdit()

		const { text, treino } = this.state
		treino.set("description", text)

		this.setState({ treino }, () => {
			this.updateTrainingOnFirebase()
		})

	}

	switchEdit = () => {
		if (!this.state.text)
			this.setState({ text: "Adicione o treino aqui" })

		this.setState({ edit: !this.state.edit })
	}

	editText = (event) => {
		this.setState({ text: event.target.value });
	}

	deleteTreino = () => {
		const { treino } = this.state
		const { athleteId, month } = this.props

		this.props.onDelete(treino)

		TrainingService.deleteTraining(athleteId, month.getBody().id, treino)
			.then(result => {
			})
			.catch(() => {
				alert("Houve um erro ao deletar o seu treino. Por gentileza, contate o setor de TI.")
			})
	}

	updateTrainingOnFirebase = () => {
		const { treino } = this.state
		const { athleteId, month } = this.props

		TrainingService.updateTraining(athleteId, month.getBody().id, treino)
			.then(result => {
			})
			.catch(() => {
				alert("Houve um erro ao atualizar a descrição do seu treino. Por gentileza, contate o setor de TI.")
			})
	}
}

class TreinoModal extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			open: false
		}
	}

	bg = {
		modal: {
			width: "80%",
			height: "40%",
			borderColor: "black",
			borderRadius: "15px",
			textAling: "center",
			padding: "0px",
		}
	};

	onOpenModal = () => {

		let a = true
		this.setState({ open: a });
	};

	onCloseModal = () => {
		let a = false

		this.setState({ open: a });
	};

	render() {
		const { open } = this.state
		const { treino } = this.props

		return <>
			<i className="fa fa-info-circle" style={{cursor: "pointer"}} onClick={this.onOpenModal} />

			<Modal open={open} onClose={this.onCloseModal} center 
				aria-labelledby="modal-title" aria-describedby="modal-description"
				styles={this.bg} >

				<div id="modalDiv">
					<h3 id="modal-title" className="modalTitulo">Semana {treino.week} | Treino {treino.order}</h3>
				</div>
				<div className="col-md-6">
					<img className="img" src="https://s2.aconvert.com/convert/p3r68-cdx67/cb2yb-s343m-001.ico" alt=''></img>
					<div id="modal-description" className="modalText">
						<strong>Distância percorrida:</strong> {treino.meters} metros
					</div>
				</div>
				<div className="col-md-6" >
					<img className="img" src="https://s2.aconvert.com/convert/p3r68-cdx67/cb2s2-n8719-001.ico" alt=''></img>
					<div id="modal-description" className="modalText">
						<strong>Tempo gasto:</strong> {LibDate.humanizeSeconds(treino.seconds)}
					</div>
				</div>
				<div className="col-md-6">
					<img className="img" src="https://s2.aconvert.com/convert/p3r68-cdx67/cby2z-uvb08-001.ico" alt=''></img>
					<div className="modalText">
						<strong>Completo em:</strong> {LibDate.formatDate(treino.completedAt.toDate())}
					</div>
				</div>
			</Modal>
		</>
	}
}