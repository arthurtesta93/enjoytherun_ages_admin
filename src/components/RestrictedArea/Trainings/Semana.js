import React, { Component } from 'react'
import Treino from './Treino';

import TrainingSchema from '../../../schemas/Training'
import TrainingService from '../../../services/TrainingService';

export default class Semana extends Component {
	constructor(props) {
		super(props);

		this.state = {
			lista: props.trainings,
			openLastTreino: false,
			loading: false
		}
	}

	componentDidUpdate(prevProps){
		if(this.props.trainings !== prevProps.trainings)
		this.setState({ lista: this.props.trainings })
	}
	
	render() {
		return (
			<table className="table table-striped table-bordered">
				<thead>
					<tr>
						<th className="semana">Semana {this.props.numero}</th>
					</tr>
				</thead>
				<tbody>
					{this.renderBody()}
				</tbody>
			</table>
		)
	}

	renderBody = () => {
		if (this.state.loading)
			return <div style={{textAlign: "center"}}>
				<img src="/assets/img/loader.gif" alt='' style={{height: "70px"}} />
			</div>

		return <>
			{this.renderTreinos()}

			<tr className="trAdd">
				<td>	
					<div className="addStyle" onClick={this.includeTreino}>
						Adicionar treino <i style={{paddingTop: "2px"}} className="fa fa-plus-circle fa-lg fa-pull-right" ></i>
					</div>
				</td>
			</tr>
		</>
	}

	renderTreinos = () => {
		const { lista, openLastTreino } = this.state
		const lastElementIndex = lista.length -1

		const { athleteId, month } = this.props

		this.orderLista(lista);

		return lista.map((obj, idx) => {
			const isLastElement = idx === lastElementIndex
			return (
				<tr key={obj.getBody().id}>
					<td>
						<Treino treino={obj} 
							month={month}
							athleteId={athleteId}
							onDelete={() => this.excludeTreino(obj)} 
							forceEdit={isLastElement && openLastTreino} />
					</td>
				</tr>
			)
		})
	}

	orderLista = (lista) =>{
		lista.sort((a, b) => 
			a.getBody().order > b.getBody().order ? 1 : -1 )
	}

	getMaxOrder = () => {
		let max = 0

		this.state.lista.forEach(training => {
			let order = training.getBody().order

			if (order > max)
				max = order
		})

		return max + 1
	}

	includeTreino = () => {
		const { athleteId, month } = this.props

		const training = new TrainingSchema({
			week: this.props.numero,
			order: this.getMaxOrder(),
			description: "Novo treino aqui..."
		})

		this.setState({ loading: true })

		TrainingService.createTraining(athleteId, month.getBody().id, training)
			.then(ref => {

				this.props.forceReload()
			})
			.catch(() => {
				alert("erro ao criar o treino")
			})
	}

	excludeTreino = (treino) => {
		const old = [...this.state.lista]

		const curr = old.filter(t => t.getBody().id !== treino.getBody().id)

		this.setState({
			lista: curr
		})
	}
}	