import React, { Component } from 'react';
import { RaisedButton } from 'material-ui'
import Mes from './Mes';
import MonthSchema from '../../../schemas/Month';
import TrainingService from '../../../services/TrainingService'

export default class Planilha extends Component {
    constructor(props) {
        super(props)

        this.state = {
            currentMonth: null,
            months: null,
            loading: true,
            forceMonthId: null
        }
    }

    componentDidMount() {
        this.fetchMonths();
    }

    fetchMonths = () =>{
        const { athleteId } = this.props

        TrainingService.getMonths(athleteId)
            .then(months => {
                this.setState({ months, loading: false }, () => {
                    this.setCurrentMonth()
                })
            })
            .catch(() => {
                alert("Não foi possível recuperar a lista de meses.")
            })
    }

    setCurrentMonth = () => {
        const { months } = this.state
        this.orderLista(months)
        if (months.length) {   //primeira vez que estou acessando a tela. quero o comportamento padrão         
            if(this.state.forceMonthId === null){
                const notFulfilledMonth = months.find(m => 
                    !m.getBody().fulfilled)
                if (notFulfilledMonth){
                    this.setState({ currentMonth: notFulfilledMonth })
                }else{
                    this.setState({ currentMonth:  months[months.length-1]})
                }
            }else{
                const currentMonth = months.find(m => m.getBody().index === this.state.forceMonthId)
                this.setState({ currentMonth });
            }
        }
        else //ou, sei qual mês quero acessar. então, acessá-lo
            this.createNextMonth();
    }



    isReadyToRender = () => {
        return this.state.currentMonth && !this.state.loading
    }

    get loader() {
        return <div style={{marginTop: "50px", textAlign: "center"}}>
            <img src="/assets/img/loader.gif" alt='' style={{height: "100px"}} />
              </div>
    }

    render() { 
        if (!this.isReadyToRender())
            return this.loader

        return (
            <div className="planilha">
                <h2><i className="fa fa-heartbeat"></i> Treinos</h2>

                <br />

                <div className="row">
                    <div className="col-md-1">
                        {this.renderPreviousMonth}
                    </div>
                    <div className="col-md-1 col-md-offset-10 text-right">
                        <RaisedButton type="button" secondary={true} color="secondary" label='Próximo' onClick={this.nextMonthClick} />
                    </div>    
                </div>

                <Mes athleteId={this.props.athleteId} month={this.state.currentMonth}/>
            </div>
        )
    }

    getIndex = () =>{
        if(!this.state.currentMonth)
            return 1;
        else
            return this.state.currentMonth.getBody().index + 1;
    }

    createNextMonth = () => {
        const { athleteId } = this.props
        const index = this.getIndex()
        
        const month = new MonthSchema({
            index: index,
            fulfilled: false
        });
        
        this.setState({ loading: true })

		TrainingService.createMonth(athleteId, month)
			.then(() => {
                this.setState({forceMonthId: index});
                this.forceReload()
			})
			.catch(error => {

				alert("Erro ao criar o mês. Por gentileza, contate o administrador.")
			})
    }

    forceReload = () => {
        this.setState({ months: null })
        this.fetchMonths();
    }
    
    orderLista = (lista) =>{
		lista.sort(function (a, b) {
			if (a.getBody().index > b.getBody().index) {
			  return 1;
			}
			if (a.getBody().index < b.getBody().index) {
			  return -1;
			}
			return 0;
		  });
	}

    //####################################
    //####################################
    //############ PAGINATION ############
    //####################################
    //####################################

    
    get renderPreviousMonth() {
        if (!this.hasThatMonth(-1))
            return null

        return <RaisedButton type="button" secondary={true} color="secondary" label='Anterior' onClick={this.previousMonthClick} />
    }

    navigateToMonths = (forwardOrBackward) => {
        let { currentMonth, months } = this.state

        let currentIndex = currentMonth.getBody().index;
        currentMonth = months.find(
            m => m.getBody().index === currentIndex + forwardOrBackward)

        this.setState({ currentMonth })
    }

    previousMonthClick = () => {
        this.navigateToMonths(-1)
    }

    nextMonthClick = () => {
        if (this.hasThatMonth(1))
            this.navigateToMonths(1)
        else
            this.createNextMonth()
    }

    hasThatMonth = (prevOrNext) =>{
        const { currentMonth, months } = this.state

        let currentIndex = currentMonth.getBody().index;
        let lastMonth = months.find(m => 
            m.getBody().index === currentIndex + prevOrNext)

        return lastMonth != null        
    }
}