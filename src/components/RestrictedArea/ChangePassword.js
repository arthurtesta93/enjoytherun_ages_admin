import React, { Component } from 'react'

import { Field, reduxForm } from 'redux-form'
import VF from '../../constants/ValidationFields'

import RaisedButton from 'material-ui/RaisedButton'

import { renderTextField } from '../../lib/MaterialUIPersonalized'

import Alert from '../../lib/Alert'

class Password extends Component {
    render() {
        const { handleSubmit } = this.props

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-xs-6">
                        <h1><i className="glyphicon glyphicon-lock"></i> {("Alterar Senha")}</h1>
                    </div>
                </div>

                <form onSubmit={handleSubmit(this.handleSubmit)}>
                    <div className="row">
                        <div className="col-md-12" >
                            <Field component={renderTextField}
                                name="oldPassword"
                                type="password"
                                hintText={("Dica: Senha123")}
                                floatingLabelText={('Senha antiga')}
                                validate={[VF.Required]} />
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6" >
                            <Field component={renderTextField}
                                name="newPassword"
                                type="password"
                                hintText={("Dica: Senha123")}
                                floatingLabelText={('Nova senha')}
                                validate={[VF.Required, VF.Password]} />
                        </div>
                        <div className="col-md-6" >
                            <Field component={renderTextField}
                                name="confirmPassword"
                                type="password"
                                hintText={("Dica: Senha123")}
                                floatingLabelText={('Confirme a nova senha')}
                                validate={[VF.Required, VF.Password]} />
                        </div>
                    </div>

                    <br />

                    <div className="row">
                        <div className="col-md-12 text-center">
                            <RaisedButton type="submit" primary={true} color="primary" label={('Salvar')} />
                        </div>
                    </div>
                </form>
            </div>
        )
    }

    handleSubmit = (values) => {
        if (values.newPassword !== values.confirmPassword)
            return Alert.error('Senhas não coincidem')

        Request.put('/user/password', { old: values.oldPassword, new: values.newPassword },
            res => {
                Alert.success('Senha alterada com sucesso')
                this.props.router.push("/")
            },
            error => {
                Alert.error(error.message)
            })
    }
}

export default reduxForm({ form: "password" })(Password)