import React, {Component} from 'react';
import Auth from '../../lib/Authentication'
import { ColumnChart, BarChart} from 'react-chartkick' 
import 'chart.js' 
import AthlService from '../../services/AthlService';


class Index extends Component {
    constructor(props){
        super(props)

        this.state = {
            athletes: [],
            trainings: null,
            data: [],
            dataTrainingComplete : []
        }

    }

    componentDidMount(){
        this.fetchAthletes()
    }


    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mgt15">
                        <h2> <i className="fa fa-area-chart "></i> {Auth.getUserName()}, seja bem-vindo(a)!</h2>
                        <br/>
                        <br/>
                            <h4>Total de treinos</h4>
                            <ColumnChart data={this.state.data} messages={{empty: "Nenhum treino cadastrado"}}  />
                            <h4>Treinos completos</h4>
                            <BarChart data={this.state.dataTrainingComplete} messages={{empty: "Nenhum treino completo"}} />
                    </div>
                </div>

                <div className="row panel-boxes mgt30"></div>
                
            </div>
        )
    }    
    
       
    fetchAthletes = () => {
        AthlService.getAll()
        .then(athletes => {
            const data = this.parseAthletesTrainingQty(athletes)
            const dataTrainingComplete = this.parseAthletesCompletedTrainings(athletes)
            this.setState({data})
            this.setState({dataTrainingComplete})
        })
        .catch(() => {
            alert("erro ao buscar lista atletas")
        })
    }
 
    parseAthletesTrainingQty = (athletes) => {
        const data = athletes.map((athlete)=> { 
            const nome = athlete.name
            const treinos = athlete.amountOfTrainings   
            const nomeAtleta = [nome,  treinos] 
            
                return nomeAtleta 
            
        })
        return data
    }

 
    parseAthletesCompletedTrainings = (athletes) => {
        const dataTrainingComplete = athletes.map((athlete) => {
            const nome = athlete.name
            const treinosCompletos = (athlete.completedTrainings || []).length
            const nomeAtleta = [nome, treinosCompletos]

                return nomeAtleta
        })
        
          return dataTrainingComplete
    }

   
}

export default Index