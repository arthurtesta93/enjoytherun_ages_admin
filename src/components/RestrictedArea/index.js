import React, {Component} from 'react';

/* Components */
import Header from './_Header';
import Footer from '../_Footer';

class RestrictedAreaIndex extends Component {
    render() {
        return (
            <div className='contentWrapper'>
                <div className='contentWrapper'>
                    <Header router={this.props.router} />

                    <main>
                        {this.props.children}
                    </main>
                </div>

                <Footer />
            </div>
        )
    }
}

export default RestrictedAreaIndex